# Starting From Beginning : Ethereum + Node WalkThrough

Just a quick app to get started on dApps

## 1. Set up environment

* Create project folder
* Install [Node](https://nodejs.org)

## 2. Install NPM packages

```npm
npm install -g ganache-cli
npm install -g truffle
```

## 3. Initiate NPM / Truffle

```npm
npm init
truffle init
```

* Now you have a contract app and can use truffle compile, truffle migrate and truffle test to compile your contracts, deploy those contracts to the network, and run their associated unit tests

## 4. Make a basic contract

* Note for Windows: truffle commands might need to be ran explicitly with truffle.cmd
* Note for MacOS: Truffle is sometimes confused by .DS_Store files. If you get an error mentioning one of those files, just delete it.

1. Create a ProofOfExistence contract
```npm
truffle create contract ProofOfExistence
```
2. Add code:
```solidity
pragma solidity ^0.4.4;

// Proof of Existence contract, version 1
contract ProofOfExistence {

  // maps are like dictionaries
  //  where here the key is bytes32 and value is a bool
  //  maps are better gas-saving alternatives to iterating through a list
  mapping (bytes32 => bool) private proofs;

  // store a proof of existence in the contract state
  // *transactional function* perform state changes
  //   when we call transactional function, we get a Promise to a transactional object, not actually what the function returns
  function storeProof(bytes32 proof) {
    proofs[proof] = true;
  }

  // helper function to get a document's sha256
  // *read-only function* marked by keyword constant
  //   read-only functions means we're not interacting with the blockchain so there is no transaction for the blockchain to execute
  function proofFor(string document) constant returns (bytes32) {
    return sha256(document);
  }

  // calculate and store the proof for a document
  // *transactional function*
  function notarize(string document) {
    var proof = proofFor(document);
    storeProof(proof);
  }

  // returns true if proof is stored
  // *read-only function*
  function hasProof(bytes32 proof) constant returns (bool) {
    return proofs[proof];
  }

  // check if a document has been notarized
  // *read-only function*
  function checkDocument(string document) constant returns (bool) {
    var proof = proofFor(document);
    return hasProof(proof);
  }

}
```

### 5. Edit Migration File for Deployment
1. Create 2_deploy_contracts.js in migrations folders
2. Add code:
```solidity
var ProofOfExistence = artifacts.require("./ProofOfExistence.sol");
module.exports = function(deployer) {
  deployer.deploy(ProofOfExistence);
};
```

### 6. Edit Truffle network configuration settings
1. Add development network to truffle.js file 
```javascript
  networks: {
    development: {
      host: "localhost",
      port: 8545,
      network_id: "*"
    }
  }
```

### 7. Run App for Basic Contract
1. In a separate command window, run the Ganache CLI
```
ganache-cli
```
2. Then compile the truffle app and run console
```
truffle compile
truffle migrate
truffle console
```
3. Test your functions - store representation of contract as variable
```solidity
var poe = ProofOfExistence.at(ProofOfExistence.address)
```
4. Create some documents for storage
```solidity
poe.notarize('hello world')
poe.notarize('goodbye everyone')
```
5. Check the document exists
```solidity
poe.checkDocument('hello world')
poe.checkDocument('asdlfjldf')
```

-----

6. Now let's create a new project and get familiar with the truffle tutorial, make a new folder and unbox webpack
```
truffle unbox webpack
```
* Note that this will automatically install npm depednencies
7. compile and migrate to truffle development
```solidity
truffle develop
compile
migrate
```
8. In another window, run the webpack front-end
```npm
npm run dev
```
9. Open a browser window and go to http://localhost:8080/, you can now test sending random coins on a sample app
10. You can also test your contract functions using the test command inside Truffle develop
```solidity
truffle develop
test
```
* This gives us a framework to start writing Dapps!


# REFERENCES

* [Basic Setup - Zeppelin Hitchikers Guide](https://blog.zeppelin.solutions/the-hitchhikers-guide-to-smart-contracts-in-ethereum-848f08001f05)
* [Truffle WebPack Tutorial](http://truffleframework.com/boxes/webpack)
