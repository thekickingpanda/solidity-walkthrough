pragma solidity ^0.4.4;

// Proof of Existence contract, version 1
contract ProofOfExistence {

  // maps are like dictionaries
  //  where here the key is bytes32 and value is a bool
  //  maps are better gas-saving alternatives to iterating through a list
  mapping (bytes32 => bool) private proofs;

  // store a proof of existence in the contract state
  // *transactional function* perform state changes
  //   when we call transactional function, we get a Promise to a transactional object, not actually what the function returns
  function storeProof(bytes32 proof) {
    proofs[proof] = true;
  }

  // helper function to get a document's sha256
  // *read-only function* marked by keyword constant
  //   read-only functions means we're not interacting with the blockchain so there is no transaction for the blockchain to execute
  function proofFor(string document) constant returns (bytes32) {
    return sha256(document);
  }

  // calculate and store the proof for a document
  // *transactional function*
  function notarize(string document) {
    var proof = proofFor(document);
    storeProof(proof);
  }

  // returns true if proof is stored
  // *read-only function*
  function hasProof(bytes32 proof) constant returns (bool) {
    return proofs[proof];
  }

  // check if a document has been notarized
  // *read-only function*
  function checkDocument(string document) constant returns (bool) {
    var proof = proofFor(document);
    return hasProof(proof);
  }

}